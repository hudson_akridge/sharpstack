﻿using System;
using System.Web.Mvc;

namespace SharpStack.Impl.MvcWeb.ActionResults
{
    public class HttpForbiddenResult : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            context.HttpContext.Response.StatusCode = 403;
        }
    }
}