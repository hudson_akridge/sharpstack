﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using SharpStack.Impl.MvcWeb.Serializations;

namespace SharpStack.Impl.MvcWeb.ActionResults
{
    public class LargeJsonResult : JsonResult
    {
        private const string JsonRequestGetNotAllowed = "This request has been blocked because sensitive information could be disclosed to third party web sites when this is used in a GET request. To allow GET requests, set JsonRequestBehavior to AllowGet.";

        public LargeJsonResult(object data)
            : this(data, JsonRequestBehavior.DenyGet)
        {
        }

        public LargeJsonResult(object data, JsonRequestBehavior behavior)
        {
            Data = data;
            JsonRequestBehavior = behavior;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            Guard(context);

            var response = context.HttpContext.Response;

            response.ContentType = !String.IsNullOrEmpty(ContentType) ? ContentType : "application/json";
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data == null) return;

            var jsonData = JsonConvert.SerializeObject(Data, Formatting.None,
                new JsonConverter[]
                    {
                        new JsonDecimalConverter("0.00"),
                    });
            response.Write(jsonData);
        }

        private void Guard(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException(JsonRequestGetNotAllowed);
            }
        }
    }
}