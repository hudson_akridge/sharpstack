﻿using System;
using System.Net.Mime;
using System.Web.Mvc;

namespace SharpStack.Impl.MvcWeb.ActionResults
{
    public class InlineableFileContentResult : FileContentResult
    {
        public InlineableFileContentResult(byte[] fileContents, string contentType)
            : this(fileContents, contentType, false)
        {
        }

        public InlineableFileContentResult(byte[] fileContents, string contentType, bool inline)
            : base(fileContents, contentType)
        {
            Inline = inline;
        }

        protected bool Inline { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            var response = context.HttpContext.Response;

            response.ContentType = ContentType;
            if (!string.IsNullOrEmpty(FileDownloadName))
            {
                var disposition = new ContentDisposition
                    {
                        FileName = FileDownloadName,
                        Inline = Inline
                    };

                var str = disposition.ToString();
                context.HttpContext.Response.AddHeader("Content-Disposition", str);
            }

            WriteFile(response);
        }
    }
}