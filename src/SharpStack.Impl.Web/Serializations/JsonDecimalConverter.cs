﻿using System;
using Newtonsoft.Json;

namespace SharpStack.Impl.MvcWeb.Serializations
{
    public class JsonDecimalConverter : JsonConverter
    {
        private readonly string _format;

        public JsonDecimalConverter(string format)
        {
            _format = format;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var stringValue = ((decimal) value).ToString(_format);
            serializer.Serialize(writer, stringValue);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (decimal);
        }
    }
}