﻿using System;
using Newtonsoft.Json.Serialization;

namespace SharpStack.Impl.MvcWeb.Serializations
{
    public class DecimalContractResolver : DefaultContractResolver
    {
        private readonly string _format;

        public DecimalContractResolver(string format)
        {
            _format = format;
        }

        protected override JsonPrimitiveContract CreatePrimitiveContract(Type objectType)
        {
            var contract = base.CreatePrimitiveContract(objectType);
            if (objectType == typeof (decimal))
            {
                contract.Converter = new JsonDecimalConverter(_format);
            }

            return contract;
        }
    }
}