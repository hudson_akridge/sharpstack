﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SharpStack.Impl.MvcWeb.Models.Authorizations
{
    [PropertiesMustMatch("NewPassword", "ConfirmPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    [Serializable]
    public class NewPasswordModel
    {
        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [DisplayName("New password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm new password")]
        public string ConfirmPassword { get; set; }
    }
}