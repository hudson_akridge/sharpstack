﻿namespace SharpStack.Impl.MvcWeb.Models.Authorizations
{
    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }
}