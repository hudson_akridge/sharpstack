﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SharpStack.Impl.MvcWeb.Models.Authorizations
{
    public class LogOnModel
    {
        [Required]
        [DisplayName("User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Remember me?")]
        public bool RememberMe { get; set; }
    }
}