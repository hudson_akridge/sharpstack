﻿using SharpStack.Impl.MvcWeb.Models.Authorizations;
using SharpStack.Security;
using SharpStack.Security.Cryptography;

namespace SharpStack.Impl.MvcWeb.Controllers
{
    public class WebSecurityComponent
    {
        private IFormsAuthenticationService _formsService;

        public virtual IFormsAuthenticationService FormsService
        {
            get { return _formsService ?? (_formsService = new FormsAuthenticationService()); }
            set { _formsService = value; }
        }

        public virtual bool IsPasswordCorrect(string submittedPassword, string hashedPassword, string salt)
        {
            return hashedPassword.MatchesAfterHashing(submittedPassword, salt);
        }
    }
}