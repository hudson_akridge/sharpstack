﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SharpStack.Impl.MvcWeb.ActionResults;
using SharpStack.Impl.Services.Dtos;

namespace SharpStack.Impl.MvcWeb.Controllers
{
    public interface IWebController
    {
        ActionResult File(byte[] fileContents, string contentType, string fileDownloadName, bool inline);
        T DeserializeJson<T>(string json);
        ActionResult Nothing();
        ActionResult ExtNewEntityJson(Guid id);
        ActionResult ExtDetailJson(object entity);
        ActionResult ExtComboBoxJson<TKey, TValue>(Dictionary<TKey, TValue> listItems);

        ActionResult ExtGridJson<T>(IEnumerable<T> data, long totalCount)
            where T : class;

        ActionResult ExtGridJson<T>(IEnumerable<T> data, long totalCount, JsonRequestBehavior behavior)
            where T : class;
    }

    public class WebControllerComponent : IWebController
    {
        private static readonly JavaScriptSerializer Jss = new JavaScriptSerializer();

        public ActionResult File(byte[] fileContents, string contentType, string fileDownloadName, bool inline)
        {
            var result = new InlineableFileContentResult(fileContents, contentType, inline)
                {
                    FileDownloadName = fileDownloadName
                };
            return result;
        }

        public T DeserializeJson<T>(string json)
        {
            return DeserializeFromJson<T>(json);
        }

        public static T DeserializeFromJson<T>(string json)
        {
            return Jss.Deserialize<T>(json);
        }

        public static string SerializeToJson(object obj)
        {
            return Jss.Serialize(obj);
        }

        public ActionResult Nothing()
        {
            return new EmptyResult();
        }

        public ActionResult ExtNewEntityJson(Guid id)
        {
            return new LargeJsonResult(new {code = id.ToString()});
        }

        public ActionResult ExtDetailJson(object entity)
        {
            return new LargeJsonResult(new {entity});
        }

        public ActionResult ExtComboBoxJson<TKey, TValue>(Dictionary<TKey, TValue> listItems)
        {
            var metaData = new {fields = new[] {"Key", "Value"}, root = "root"};
            return new LargeJsonResult(new {root = listItems.ToList(), metaData});
        }

        public ActionResult ExtGridJson<T>(IEnumerable<T> data, long totalCount)
            where T : class
        {
            return ExtGridJson(data, totalCount, JsonRequestBehavior.DenyGet);
        }

        public ActionResult ExtGridJson<T>(IEnumerable<T> data, long totalCount, JsonRequestBehavior behavior) where T : class
        {
            var flattenedDtos = (data.Any())
                                    ? data.Select(FlattenDto)
                                    : new Collection<Field[]>();

            var fields = GetFields<T>().Select(x => new {name = x.Name, mapping = x.Mapping});

            var root = (flattenedDtos.Any())
                           ? flattenedDtos.Select(f => f.ToDictionary(x => x.Mapping, x => x.Value))
                           : new Collection<Dictionary<string, object>>();

            var metaData = new {fields, root = "root", totalProperty = "totalCount"};
            return new LargeJsonResult(new {root, totalCount, metaData}, behavior);
        }

        private static IEnumerable<Field> GetFields<T>()
        {
            var dataFields = GetDataFieldsForMetaData(typeof (T), string.Empty, string.Empty, 0);
            var allFields = new Collection<Field>(dataFields).ToList();

            return allFields;
        }

        private static Field[] FlattenDto(object dto)
        {
            var dtoCollection = new Collection<Field>();

            foreach (var fieldName in GetDataFields(dto.GetType()))
            {
                var propertyInfo = dto.GetType().GetProperty(fieldName.Name);
                var fieldValue = propertyInfo.GetValue(dto, null) ?? string.Empty;

                dtoCollection.Add(new Field(fieldName.Name, fieldName.Name, fieldValue));
            }

            return dtoCollection.ToArray();
        }

        private static IEnumerable<Field> GetDataFields(Type dtoType)
        {
            return dtoType.GetProperties()
                .Select(propertyInfo => new Field(propertyInfo.Name, propertyInfo.Name, string.Empty))
                .ToArray();
        }

        private static Field[] GetDataFieldsForMetaData(Type dtoType, string namePrefix, string mappingPrefix, int level)
        {
            var fields = new List<Field>();
            foreach (var propertyInfo in dtoType.GetProperties())
            {
                var localNamePrefix = (level > 0) ? namePrefix + "." : namePrefix;
                var localMappingPrefix = (level > 0) ? mappingPrefix + "['" : mappingPrefix;
                var localMappingSuffix = (level > 0) ? "']" : string.Empty;
                var localMappingName = localMappingPrefix + propertyInfo.Name + localMappingSuffix;
                if (propertyInfo.GetCustomAttributes(typeof (ShouldSerializeDto), true).Any())
                {
                    if (level > 1)
                    {
                        continue;
                    } //Do not add more than two levels deep.
                    fields.AddRange(GetDataFieldsForMetaData(propertyInfo.PropertyType, localNamePrefix + propertyInfo.Name, localMappingName, level + 1));
                }
                else
                {
                    var name = localNamePrefix + propertyInfo.Name;
                    fields.Add(new Field(name, name, string.Empty));
                }
            }
            return fields.ToArray();
        }

        private class Field
        {
            public Field(string name, string mapping, object value)
            {
                Name = name;
                Mapping = mapping;
                Value = value;
            }

            public string Name { get; private set; }
            public string Mapping { get; private set; }
            public object Value { get; private set; }

            public override int GetHashCode()
            {
                return (Name + Mapping + Value).GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (obj.GetType() != GetType())
                {
                    return false;
                }
                var o = (Field) obj;
                return Name == o.Name
                       && Mapping == o.Mapping
                       && Value == o.Value;
            }

            public static bool operator ==(Field o1, Field o2)
            {
                return o1.Equals(o2);
            }

            public static bool operator !=(Field o1, Field o2)
            {
                return !(o1 == o2);
            }
        }
    }
}