﻿using System;
using System.Web.Mvc;
using SharpStack.Impl.MvcWeb.Models.Authorizations;

namespace SharpStack.Impl.MvcWeb.Controllers
{
    public interface IBasicAuthorizationController
    {
        ActionResult Login();
        ActionResult LogOn(LogOnModel model, string returnUrl);
        ActionResult LogOff();
        ActionResult ForgotPassword();
        ActionResult ForgotPassword(string userName);
        ActionResult ForgotPasswordSent();
        ActionResult PasswordReset(string userName, Guid? token);
        ActionResult PasswordReset(string userName, Guid? token, string newPassword, string confirmPassword);
    }
}