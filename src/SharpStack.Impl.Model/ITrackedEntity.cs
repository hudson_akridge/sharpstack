﻿namespace SharpStack.Impl.Model
{
    public interface ITrackedEntity : ITrackedClass
    {
        string Name { get; set; }
    }
}