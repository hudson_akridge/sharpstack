﻿using System;

namespace SharpStack.Impl.Model
{
    public abstract class TrackedClass : ITrackedClass
    {
        protected TrackedClass()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
        }

        public virtual Guid Id { get; protected internal set; }
        public virtual int Version { get; protected internal set; }
        public virtual DateTime CreatedOn { get; protected internal set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is TrackedClass)) return false;

            var trackedEntity = obj as TrackedClass;
            return trackedEntity.Id == Id;
        }

        public override int GetHashCode()
        {
            return (GetType().FullName + Id).GetHashCode();
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public static bool operator ==(TrackedClass a, TrackedClass b)
        {
            if (ReferenceEquals(a, b)) return true;
            if (((object) a == null) || ((object) b == null)) return false;

            return Equals(a, b);
        }

        public static bool operator !=(TrackedClass a, TrackedClass b)
        {
            return !(a == b);
        }
    }
}