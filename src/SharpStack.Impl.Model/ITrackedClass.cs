﻿using System;

namespace SharpStack.Impl.Model
{
    public interface ITrackedClass
    {
        Guid Id { get; }
        int Version { get; }
        DateTime CreatedOn { get; }
    }
}