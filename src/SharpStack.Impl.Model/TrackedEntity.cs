﻿namespace SharpStack.Impl.Model
{
    public abstract class TrackedEntity : TrackedClass, ITrackedEntity
    {
        public virtual string Name { get; set; }
    }
}