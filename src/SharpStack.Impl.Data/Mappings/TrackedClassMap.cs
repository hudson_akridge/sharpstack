﻿using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using FluentNHibernate.Mapping;
using SharpStack.Impl.Model;

namespace SharpStack.Impl.Data.Mappings
{
    public abstract class TrackedClassMap<T> : ClassMap<T> where T : ITrackedClass
    {
        protected IdentityPart IdProperty;

        protected TrackedClassMap()
        {
            var mappedType = typeof (T);
            var className = AlterClassName(mappedType.Name);
            var tableName = AlterTableName(className);
            Table(tableName);
            IdProperty = Id(x => x.Id, className + "ID").GeneratedBy.Assigned();
            Version(x => x.Version).UnsavedValue("0");
            Map(x => x.CreatedOn);
        }

        /// <summary>
        /// The default of this method removes the word "Base" from the end of a class name
        /// </summary>
        /// <param name="originalClassName"></param>
        /// <returns></returns>
        protected virtual string AlterClassName(string originalClassName)
        {
            if (originalClassName.Length <= 4)
            {
                return originalClassName;
            }

            var lastFourCharacters = originalClassName.Substring(originalClassName.Length - 4, 4);
            var isBase = (lastFourCharacters.Equals("base", StringComparison.InvariantCultureIgnoreCase));
            return !isBase
                       ? originalClassName
                       : originalClassName.Replace(lastFourCharacters, string.Empty);
        }

        /// <summary>
        /// The default behavior is to pluralize the class name that's passed in
        /// </summary>
        /// <param name="originalTableName"></param>
        /// <returns></returns>
        protected virtual string AlterTableName(string originalTableName)
        {
            var pluralizer = PluralizationService.CreateService(CultureInfo.GetCultureInfo("en-US"));
            return pluralizer.Pluralize(originalTableName);
        }
    }
}