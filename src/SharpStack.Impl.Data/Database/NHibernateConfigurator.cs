﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping;
using NHibernate.Tool.hbm2ddl;
using SharpStack.Impl.EnvironmentConfig;
using Environment = NHibernate.Cfg.Environment;

namespace SharpStack.Impl.Data.Database
{
    public static class NHibernateConfigurator
    {
        private static Configuration _config;
        private static ISessionFactory _sessionFactory;
        private static string _connectionString;
        private static readonly string SqlServer = AppConfig.GetConfigElement(AppConfig.SqlServer);
        private static readonly string DbName = AppConfig.GetConfigElement(AppConfig.DbName);
        private static readonly string UserName = AppConfig.GetConfigElement(AppConfig.UserName);
        private static readonly string Password = AppConfig.GetConfigElement(AppConfig.Password);

        public static void Initialize<TMappingClass>()
        {
            Initialize<TMappingClass>(false, Enumerable.Empty<IAuxiliaryDatabaseObject>());
        }

        public static void Initialize<TMappingClass>(bool configureAuxiliaryObjects, IEnumerable<IAuxiliaryDatabaseObject> auxiliaryDatabaseObjects)
        {
            Initialize<TMappingClass>(configureAuxiliaryObjects, auxiliaryDatabaseObjects, false);
        }

        public static void Initialize<TMappingClass>(bool configureAuxiliaryObjects, IEnumerable<IAuxiliaryDatabaseObject> auxiliaryDatabaseObjects, bool exportMappings)
        {
            if (_config != null)
            {
                return;
            }

            _config =
                Fluently.Configure()
                    .ExposeConfiguration(x =>
                        {
                            if (!configureAuxiliaryObjects)
                            {
                                return;
                            }

                            foreach (var auxiliaryDatabaseObject in auxiliaryDatabaseObjects)
                            {
                                x.AddAuxiliaryDatabaseObject(auxiliaryDatabaseObject);
                            }
                        })
                    .Database(MsSqlConfiguration.MsSql2005
                        .ConnectionString(c =>
                                          c.Database(DbName)
                                              .Server(SqlServer)
                                              .Username(UserName)
                                              .Password(Password)
                        ))
                    .Mappings(x =>
                        {
                            var container = x.FluentMappings.AddFromAssemblyOf<TMappingClass>();
                            if (exportMappings) container.ExportTo(Directory.GetCurrentDirectory());
                        }
                    )
                    .BuildConfiguration();

            _sessionFactory = _config.BuildSessionFactory();
            _connectionString = _config.GetProperty(Environment.ConnectionString);
        }

        public static ISession GetNewSession()
        {
            if (_sessionFactory == null)
            {
                throw new InvalidOperationException("Cannot create a new session until configuration has been initialized");
            }

            return _sessionFactory.OpenSession();
        }

        public static string GetSchema()
        {
            var schemaExport = new SchemaExport(_config);
            var script = new StringBuilder();
            schemaExport.Create(x => script.AppendLine(x), false);

            return script.ToString();
        }

        public static Configuration GetConfiguration()
        {
            return _config;
        }

        public static string GetConnectionString()
        {
            return _connectionString;
        }
    }
}