﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using NHibernate.Tool.hbm2ddl;

namespace SharpStack.Impl.Data.Database
{
    public static class DatabaseUtility
    {
        public static void EmptyDatabase()
        {
            using (var sqlConnection = new SqlConnection(NHibernateConfigurator.GetConnectionString()))
            using (var sqlCommand = new SqlCommand("SELECT name FROM sys.Tables", sqlConnection))
            {
                sqlConnection.Open();

                var tableNames = new Collection<string>();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tableNames.Add(reader["name"].ToString());
                    }
                    reader.Close();
                }

                foreach (var tableName in tableNames)
                {
                    sqlCommand.CommandText = string.Format("ALTER TABLE {0} NOCHECK CONSTRAINT ALL", tableName);
                    sqlCommand.ExecuteNonQuery();
                }

                foreach (var tableName in tableNames)
                {
                    sqlCommand.CommandText = string.Format("DELETE FROM {0}", tableName);
                    sqlCommand.ExecuteNonQuery();
                }

                foreach (var tableName in tableNames)
                {
                    sqlCommand.CommandText = string.Format("ALTER TABLE {0} WITH CHECK CHECK CONSTRAINT ALL", tableName);
                    sqlCommand.ExecuteNonQuery();
                }

                sqlConnection.Close();
            }
        }

        public static void CreateSchema()
        {
            using (var conn = new SqlConnection(NHibernateConfigurator.GetConnectionString()))
            {
                conn.Open();
                new SchemaExport(NHibernateConfigurator.GetConfiguration()).Execute(false, true, false, conn, null);
                conn.Close();
            }
        }

        public static void DropSchema()
        {
            const string fkSelect = "SELECT TABLE_NAME, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'";
            const string tblSelect = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES";

            using (var conn = new SqlConnection(NHibernateConfigurator.GetConnectionString()))
            {
                conn.Open();

                // Drop FKeys
                var tableInfos = new List<TableInfo>();

                var fkCmd = new SqlCommand(fkSelect, conn);
                var fkReader = fkCmd.ExecuteReader();
                while (fkReader.Read())
                {
                    var tableInfo = new TableInfo {Table = fkReader[0].ToString(), Constraint = fkReader[1].ToString()};
                    tableInfos.Add(tableInfo);
                }
                fkReader.Close();

                foreach (var info in tableInfos)
                {
                    var alter = string.Format("ALTER TABLE [{0}] DROP CONSTRAINT [{1}]", info.Table, info.Constraint);

                    var dropFkCmd = new SqlCommand(alter, conn);
                    dropFkCmd.ExecuteNonQuery();
                }


                // Drop Tables
                tableInfos.Clear();

                var tblCmd = new SqlCommand(tblSelect, conn);
                var tblReader = tblCmd.ExecuteReader();
                while (tblReader.Read())
                {
                    var tableInfo = new TableInfo {Table = tblReader[0].ToString()};
                    tableInfos.Add(tableInfo);
                }
                tblReader.Close();

                foreach (var info in tableInfos)
                {
                    var drop = string.Format("DROP TABLE [{0}]", info.Table);

                    var dropTblCmd = new SqlCommand(drop, conn);
                    dropTblCmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        private struct TableInfo
        {
            public string Constraint;
            public string Table;
        }
    }
}