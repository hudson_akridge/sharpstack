﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using NHibernate;
using SharpStack.Core;
using SharpStack.Core.UnitsOfWork;
using SharpStack.Impl.Data.Database;

namespace SharpStack.Impl.Data.UnitsOfWork
{
    public class NHibernateUnitOfWork : IUnitOfWork
    {
        protected readonly Guid UowId = Guid.NewGuid();

        public ITransaction Transaction { get; private set; }
        private ISession Session { get; set; }

        private readonly string _userName;

        public NHibernateUnitOfWork(string userName)
            : this(AccessTypes.ReadWrite, userName)
        {
        }

        public NHibernateUnitOfWork(AccessTypes accessType, string userName)
        {
            Session = NHibernateConfigurator.GetNewSession();
            Session.FlushMode = accessType.GetNHibernateFlushMode();
            Transaction = Session.BeginTransaction();
            _userName = userName;
        }

        public virtual void Save(object entity)
        {
            Session.SaveOrUpdate(entity);
        }

        public void Save(IEnumerable<object> entities)
        {
            foreach (var entity in entities)
            {
                Save(entity);
            }
        }

        public virtual T Load<T>(Guid id)
        {
            return Session.Load<T>(id);
        }

        public virtual SqlConnection GetConnection()
        {
            return (SqlConnection) Session.Connection;
        }

        public virtual ITransaction GetTransaction()
        {
            return Transaction;
        }

        public virtual ISession GetSession()
        {
            return Session;
        }

        public virtual ICriteria GetCriteria<T>() where T : class
        {
            return Session.CreateCriteria<T>();
        }

        public virtual IMultiCriteria GetMultiCriteria()
        {
            return Session.CreateMultiCriteria();
        }

        public virtual void Delete(object entity)
        {
            Session.Delete(entity);
        }

        public void Delete(IEnumerable<object> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public virtual void CommitTransaction()
        {
            var transaction = Session.Transaction;
            try
            {
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw;
            }
        }

        public virtual void FlushToDatabase()
        {
            Session.Flush();
        }

        public virtual string UserName
        {
            get { return _userName; }
        }

        protected bool Disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (Disposed) return;
            if (disposing)
            {
                if (Session.Transaction != null && Session.Transaction.IsActive)
                {
                    Session.Transaction.Rollback();
                }
                Session.Dispose();
            }

            Disposed = true;
        }

        protected virtual void ThrowIfDisposed()
        {
            if (Disposed) throw new ObjectDisposedException(UowId.ToString(), "UnitOfWork disposed.");
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}