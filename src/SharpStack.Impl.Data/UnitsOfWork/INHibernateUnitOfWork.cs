﻿using NHibernate;
using SharpStack.Core;
using SharpStack.Core.UnitsOfWork;

namespace SharpStack.Impl.Data.UnitsOfWork
{
    public interface INHibernateUnitOfWork : IUnitOfWork
    {
        ITransaction GetTransaction();
        ISession GetSession();
        ICriteria GetCriteria<T>() where T : class;
        IMultiCriteria GetMultiCriteria();
    }
}