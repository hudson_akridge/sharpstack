﻿using System;
using NHibernate;
using SharpStack.Core;
using SharpStack.Core.UnitsOfWork;

namespace SharpStack.Impl.Data.UnitsOfWork
{
    public static class AccessTypesExtensions
    {
        public static FlushMode GetNHibernateFlushMode(this AccessTypes accessType)
        {
            switch (accessType)
            {
                case AccessTypes.ReadWrite:
                    return FlushMode.Auto;
                case AccessTypes.ReadOnly:
                    return FlushMode.Never;
                default:
                    throw new ArgumentOutOfRangeException("accessType");
            }
        }
    }
}