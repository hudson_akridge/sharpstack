﻿using System;
using System.Collections.Generic;
using NHibernate.Criterion;
using SharpStack.Core;
using SharpStack.Impl.Data.UnitsOfWork;
using SharpStack.Impl.Model;
using SharpStack.Security;
using SharpStack.Security.Queries;

namespace SharpStack.Impl.Data.QueryObjects
{
    public abstract class AuthorizedUserQueryObjectBase<TModel, TQueryObject> : TrackedEntityQueryObjectBase<TModel, TQueryObject>, IAuthorizedUserQueryObject
        where TModel : class, IAuthorizedUser, ITrackedEntity
        where TQueryObject : IAuthorizedUserQueryObject
    {
        protected AuthorizedUserQueryObjectBase(INHibernateUnitOfWork uow)
            : base(uow)
        {
        }

        public TQueryObject RestrictByEmail(string email)
        {
            Criteria.Add(Restrictions.InsensitiveLike(PropertyReflector.GetName<IAuthorizedUser>(x => x.Email), email, MatchMode.Anywhere));
            return (TQueryObject) (object) this;
        }

        public IAuthorizedUserQueryObject RestrictByAuthUserId(Guid id)
        {
            return base.RestrictById(id);
        }

        public IAuthorizedUserQueryObject RestrictByAuthUserIds(IEnumerable<Guid> ids)
        {
            return base.RestrictByIds(ids);
        }

        public IAuthorizedUserQueryObject RestrictByAuthUserName(string name)
        {
            return base.RestrictByName(name);
        }

        public IAuthorizedUserQueryObject RestrictByAuthUserEmail(string email)
        {
            return RestrictByEmail(email);
        }

        public IEnumerable<IAuthorizedUser> ListAllAuthUsers()
        {
            return base.ListAll();
        }

        public IAuthorizedUser GetSingleAuthUser()
        {
            return base.GetSingle();
        }
    }
}