﻿using NHibernate.Criterion;
using SharpStack.Core;
using SharpStack.Impl.Data.UnitsOfWork;
using SharpStack.Impl.Model;
using SharpStack.Impl.Services.Queries;

namespace SharpStack.Impl.Data.QueryObjects
{
    public abstract class TrackedEntityQueryObjectBase<TModel, TQueryObject> : TrackedClassQueryObjectBase<TModel, TQueryObject>,
                                                                               ITrackedEntityQueryObject<TModel, TQueryObject>
        where TModel : class, ITrackedEntity
    {
        protected readonly string NameProperty = PropertyReflector.GetName<TrackedEntity>(x => x.Name);

        protected TrackedEntityQueryObjectBase(INHibernateUnitOfWork uow)
            : base(uow)
        {
        }

        public virtual TQueryObject RestrictByName(string name)
        {
            Criteria.Add(Restrictions.Eq(NameProperty, name));
            return (TQueryObject) (object) this;
        }

        public virtual TQueryObject RestrictByNameContains(string name)
        {
            Criteria.Add(Restrictions.InsensitiveLike(NameProperty, name, MatchMode.Anywhere));
            return (TQueryObject) (object) this;
        }

        public virtual TQueryObject OrderByName(bool ascending = true)
        {
            Criteria.AddOrder(new Order(NameProperty, ascending));
            return (TQueryObject) (object) this;
        }
    }
}