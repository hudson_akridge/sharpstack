﻿using SharpStack.Security;
using SharpStack.Security.Queries;

namespace SharpStack.Impl.Data.QueryObjects.Factories
{
    public interface IAuthorizedUserQueryObjectFactory : IQueryObjectFactory<IAuthorizedUser, IAuthorizedUserQueryObject>
    {
    }
}