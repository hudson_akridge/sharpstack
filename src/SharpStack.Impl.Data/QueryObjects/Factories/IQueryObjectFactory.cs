﻿using SharpStack.Core;
using SharpStack.Core.UnitsOfWork;

namespace SharpStack.Impl.Data.QueryObjects.Factories
{
    public interface IQueryObjectFactory<TModel, out TQueryObject>
    {
        TQueryObject GetQueryObject(IUnitOfWork uow);
    }
}