﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using SharpStack.Core;
using SharpStack.Core.Services.Requests;
using SharpStack.Impl.Data.Criterias;
using SharpStack.Impl.Data.UnitsOfWork;
using SharpStack.Impl.Model;
using SharpStack.Impl.Services.Queries;

namespace SharpStack.Impl.Data.QueryObjects
{
    public abstract class TrackedClassQueryObjectBase<TModel, TQueryObject> : ITrackedClassQueryObject<TModel, TQueryObject> where TModel : class, ITrackedClass
    {
        protected INHibernateUnitOfWork Uow { get; private set; }
        protected ICommonsCriteria Criteria { get; private set; }
        protected internal static string IdProperty { get; private set; }

        protected TrackedClassQueryObjectBase(INHibernateUnitOfWork uow)
        {
            Uow = uow;
            Criteria = new LargeParameterListSafeCriteria<TModel>(uow.GetSession());
            IdProperty = PropertyReflector.GetName<TrackedEntity>(x => x.Id);
        }

        public virtual IListResult<TModel> ListPaged(int start, int limit)
        {
            Criteria.SetFirstResult(start)
                .SetMaxResults(limit);
            return GetListResult<TModel>();
        }

        public virtual IListResult<TModel> ListPaged(ListOptions options)
        {
            Criteria.SetFirstResult(options.Start)
                .SetMaxResults(options.Limit);
            return GetListResult<TModel>();
        }

        public virtual IEnumerable<TModel> ListAll()
        {
            return Criteria.List<TModel>();
        }

        public virtual IEnumerable<Guid> ListAllIds()
        {
            return Criteria
                .SetProjection(Projections.Id())
                .List<Guid>();
        }

        public virtual TModel GetSingle()
        {
            return Criteria.UniqueResult<TModel>();
        }

        public virtual TQueryObject RestrictById(Guid id)
        {
            Criteria.Add(Restrictions.Eq(PropertyReflector.GetName<TModel>(x => x.Id), id));
            return (TQueryObject) (object) this;
        }

        public virtual TQueryObject RestrictByIds(IEnumerable<Guid> ids)
        {
            Criteria.Add(Restrictions.In(PropertyReflector.GetName<TModel>(x => x.Id), ids.ToArray()));
            return (TQueryObject) (object) this;
        }

        public TQueryObject OmitByIds(IEnumerable<Guid> ids)
        {
            Criteria.Add(Restrictions.Not(Restrictions.In(PropertyReflector.GetName<TModel>(x => x.Id), ids.ToArray())));
            return (TQueryObject) (object) this;
        }

        protected virtual IListResult<TResult> GetListResult<TResult>() where TResult : class, ITrackedClass
        {
            if (!Criteria.CanAddAdditionalCriterions())
            {
                int totalResults;
                var results = Criteria.List<TResult>(out totalResults);
                return new ListResult<TResult>(totalResults, results.ToArray());
            }

            var countCriteria = ((ICriteria) Criteria.Clone())
                .SetProjection(Projections.RowCount())
                .SetFirstResult(0)
                .SetMaxResults(int.MaxValue);
            countCriteria.ClearOrders();

            var resultCriteria = ((ICriteria) Criteria.Clone());

            return GetListResult<TResult>(countCriteria, resultCriteria);
        }

        private IListResult<TResult> GetListResult<TResult>(ICriteria countCriteria, ICriteria resultCriteria) where TResult : class, ITrackedClass
        {
            var multiCriteria = Uow.GetMultiCriteria()
                .Add(countCriteria)
                .Add(resultCriteria);

            var resultSets = multiCriteria.List();
            var countResultSet = (IList) resultSets[0];
            var recordResultSet = (IList) resultSets[1];

            long total = (int) countResultSet[0];
            return new ListResult<TResult>(total, recordResultSet.Cast<TResult>().ToList());
        }
    }
}