﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using SharpStack.Impl.Model;

namespace SharpStack.Impl.Data.Criterias
{
    public static class InExpressionExtensions
    {
        public static string GetPropertyName(this InExpression inExpression)
        {
            var expressionText = inExpression.ToString();
            var propertyEndIndex = expressionText.IndexOf(" in (", StringComparison.InvariantCultureIgnoreCase);
            return expressionText.Substring(0, propertyEndIndex);
        }
    }

    public class LargeParameterListSafeCriteria<TModel> : ICommonsCriteria
        where TModel : class, ITrackedClass
    {
        private const int MaxParameterCount = 2050;
        private readonly ISession _session;
        private readonly ICollection<string> _createdAliases;
        private readonly ICriteria _wrappedCriteria;
        private InExpression _inRestriction;

        private int? _maxResults;
        private int? _firstResult;

        public LargeParameterListSafeCriteria(ISession session)
        {
            _session = session;
            _wrappedCriteria = _session.CreateCriteria<TModel>().SetResultTransformer(Transformers.DistinctRootEntity);
            _createdAliases = new HashSet<string>();
        }

        public virtual bool CanAddAdditionalCriterions()
        {
            return _inRestriction == null;
        }

        public virtual ICriteria Add(ICriterion expression)
        {
            var restriction = expression as InExpression;
            if (restriction != null)
            {
                if (_inRestriction != null)
                {
                    throw new InvalidOperationException("Only one IN Restriction is allowed.");
                }

                _inRestriction = restriction;
                return this;
            }

            _wrappedCriteria.Add(expression);
            return this;
        }

        public virtual ICriteria SetMaxResults(int maxResults)
        {
            _maxResults = maxResults;
            _wrappedCriteria.SetMaxResults(maxResults);
            return this;
        }

        public virtual ICriteria SetFirstResult(int firstResult)
        {
            _firstResult = firstResult;
            _wrappedCriteria.SetFirstResult(firstResult);
            return this;
        }

        public virtual ICriteria CreateAlias(string associationPath, string alias)
        {
            return CreateAlias(associationPath, alias, JoinType.LeftOuterJoin);
        }

        public virtual ICriteria CreateAlias(string associationPath, string alias, JoinType joinType)
        {
            if (_createdAliases.Contains(associationPath)) return this;

            _createdAliases.Add(associationPath);
            _wrappedCriteria.CreateAlias(associationPath, alias, joinType);

            return this;
        }

        public ICriteria CreateAlias(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            return _wrappedCriteria.CreateAlias(associationPath, alias, joinType, withClause);
        }

        public ICriteria SetReadOnly(bool readOnly)
        {
            return _wrappedCriteria.SetReadOnly(readOnly);
        }

        public virtual void List(IList results)
        {
            foreach (var result in List())
            {
                results.Add(result);
            }
        }

        public virtual IList<T> List<T>()
        {
            int throwAway;
            return List<T>(out throwAway);
        }

        public virtual IList<T> List<T>(out int totalResults)
        {
            if (CanAddAdditionalCriterions())
            {
                totalResults = -1;
                return _wrappedCriteria.List<T>();
            }

            var results = LargeParameterList<T>().ToArray();
            totalResults = results.Count();
            var isPaged = (_maxResults.HasValue && _firstResult.HasValue) && (_maxResults != int.MaxValue && _firstResult != int.MinValue);
            if (isPaged)
            {
                return results
                    .Skip(_firstResult.Value)
                    .Take(_maxResults.Value)
                    .ToList();
            }

            return results.ToList();
        }

        public virtual IList List()
        {
            int throwAway;
            return List(out throwAway);
        }

        public virtual IList List(out int totalResults)
        {
            return (IList) List<TModel>(out totalResults);
        }

        private IEnumerable<T> LargeParameterList<T>()
        {
            _wrappedCriteria.SetMaxResults(int.MaxValue);
            _wrappedCriteria.SetFirstResult(0);

            var results = new List<T>();
            var parameterGroupings = GroupParameters();
            foreach (var partialResults in parameterGroupings.Select(ListSegmented<T>))
            {
                results.AddRange(partialResults);
            }

            return results.Distinct();
        }

        private IEnumerable<IEnumerable<object>> GroupParameters()
        {
            var parameters = _inRestriction.Values;
            if (parameters.Count() <= MaxParameterCount)
            {
                return new List<IEnumerable<object>>(new[] {parameters});
            }

            var groups = new List<IEnumerable<object>>();
            var position = 0;
            while (position <= parameters.Count())
            {
                var group = parameters.Skip(position).Take(MaxParameterCount);
                position += MaxParameterCount;
                groups.Add(group);
            }
            return groups;
        }

        private IEnumerable<T> ListSegmented<T>(IEnumerable<object> parameters)
        {
            var criteria = CloneWrappedCriteria()
                .Add(Restrictions.In(_inRestriction.GetPropertyName(), parameters.ToArray()))
                .SetResultTransformer(Transformers.DistinctRootEntity);

            return criteria.List<T>();
        }

        private ICriteria CloneWrappedCriteria()
        {
            return (ICriteria) _wrappedCriteria.Clone();
        }

        public virtual object Clone()
        {
            if (!CanAddAdditionalCriterions())
            {
                throw new InvalidOperationException(@"Error: Cannot clone a LargeParameterList Criteria when there's already an IN criterion on it.");
            }
            return _wrappedCriteria.Clone();
        }

        public virtual ICriteria SetProjection(params IProjection[] projection)
        {
            _wrappedCriteria.SetProjection(projection);
            return this;
        }

        public virtual ICriteria AddOrder(Order order)
        {
            _wrappedCriteria.AddOrder(order);
            return this;
        }

        public virtual ICriteria SetFetchMode(string associationPath, FetchMode mode)
        {
            _wrappedCriteria.SetFetchMode(associationPath, mode);
            return this;
        }

        public virtual ICriteria SetLockMode(LockMode lockMode)
        {
            _wrappedCriteria.SetLockMode(lockMode);
            return this;
        }

        public virtual ICriteria SetLockMode(string alias, LockMode lockMode)
        {
            _wrappedCriteria.SetLockMode(alias, lockMode);
            return this;
        }

        public virtual ICriteria CreateCriteria(string associationPath)
        {
            return _wrappedCriteria.CreateCriteria(associationPath);
        }

        public virtual ICriteria CreateCriteria(string associationPath, JoinType joinType)
        {
            return _wrappedCriteria.CreateCriteria(associationPath, joinType);
        }

        public virtual ICriteria CreateCriteria(string associationPath, string alias)
        {
            return _wrappedCriteria.CreateCriteria(associationPath, alias);
        }

        public virtual ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType)
        {
            return _wrappedCriteria.CreateCriteria(associationPath, alias, joinType);
        }

        public ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            return _wrappedCriteria.CreateCriteria(associationPath, alias, joinType, withClause);
        }

        public virtual ICriteria SetResultTransformer(IResultTransformer resultTransformer)
        {
            _wrappedCriteria.SetResultTransformer(resultTransformer);
            return this;
        }

        public virtual ICriteria SetFetchSize(int fetchSize)
        {
            _wrappedCriteria.SetFetchSize(fetchSize);
            return this;
        }

        public virtual ICriteria SetTimeout(int timeout)
        {
            _wrappedCriteria.SetTimeout(timeout);
            return this;
        }

        public virtual ICriteria SetCacheable(bool cacheable)
        {
            _wrappedCriteria.SetCacheable(cacheable);
            return this;
        }

        public virtual ICriteria SetCacheRegion(string cacheRegion)
        {
            _wrappedCriteria.SetCacheRegion(cacheRegion);
            return this;
        }

        public virtual ICriteria SetComment(string comment)
        {
            _wrappedCriteria.SetComment(comment);
            return this;
        }

        public virtual ICriteria SetFlushMode(FlushMode flushMode)
        {
            _wrappedCriteria.SetFlushMode(flushMode);
            return this;
        }

        public virtual ICriteria SetCacheMode(CacheMode cacheMode)
        {
            _wrappedCriteria.SetCacheMode(cacheMode);
            return this;
        }

        public virtual object UniqueResult()
        {
            return _wrappedCriteria.UniqueResult();
        }

        public virtual T UniqueResult<T>()
        {
            return _wrappedCriteria.UniqueResult<T>();
        }

        public virtual IEnumerable<T> Future<T>()
        {
            return _wrappedCriteria.Future<T>();
        }

        public virtual IFutureValue<T> FutureValue<T>()
        {
            return _wrappedCriteria.FutureValue<T>();
        }

        public virtual void ClearOrders()
        {
            _wrappedCriteria.ClearOrders();
        }

        public virtual ICriteria GetCriteriaByPath(string path)
        {
            return _wrappedCriteria.GetCriteriaByPath(path);
        }

        public virtual ICriteria GetCriteriaByAlias(string alias)
        {
            return _wrappedCriteria.GetCriteriaByAlias(alias);
        }

        public virtual Type GetRootEntityTypeIfAvailable()
        {
            return _wrappedCriteria.GetRootEntityTypeIfAvailable();
        }

        public virtual string Alias
        {
            get { return _wrappedCriteria.Alias; }
        }

        public bool IsReadOnlyInitialized { get; private set; }
        public bool IsReadOnly { get; private set; }
    }
}