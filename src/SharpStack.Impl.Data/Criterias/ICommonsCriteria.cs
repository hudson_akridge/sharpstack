﻿using System.Collections;
using System.Collections.Generic;
using NHibernate;

namespace SharpStack.Impl.Data.Criterias
{
    public interface ICommonsCriteria : ICriteria
    {
        bool CanAddAdditionalCriterions();
        IList List(out int totalResults);
        IList<T> List<T>(out int totalResults);
    }
}