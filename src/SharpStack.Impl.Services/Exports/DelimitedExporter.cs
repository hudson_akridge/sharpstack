﻿using System.Collections.Generic;
using System.IO;
using FileHelpers;
using SharpStack.Impl.Services.Dtos;

namespace SharpStack.Impl.Services.Exports
{
    public static class DelimitedExporter
    {
        public static byte[] ToFileService<TDto>(string headerText, IEnumerable<TDto> dtos)
            where TDto : class, IDto
        {
            var engine = new FileHelperEngine<TDto>
                {
                    HeaderText = headerText,
                };

            var stream = new MemoryStream();
            var sw = new StreamWriter(stream);

            engine.WriteStream(sw, dtos);
            sw.Flush();
            stream.Position = 0;
            return stream.ToArray();
        }
    }
}