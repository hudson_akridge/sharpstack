﻿using System.Collections.Generic;
using SharpStack.Core.Services.Responses;
using SharpStack.Impl.Services.Dtos;
using SharpStack.Impl.Services.Queries;

namespace SharpStack.Impl.Services.RequestResponse.Responses
{
    public class ListResponse<TModel, TDto> : ResponseBase
        where TModel : class
        where TDto : class, IDto
    {
        public ListResponse(long totalCount, IEnumerable<TModel> models, IMapper<TModel, TDto> mapper)
        {
            MapToDto(totalCount, models, mapper);
        }

        protected virtual void MapToDto(long totalCount, IEnumerable<TModel> models, IMapper<TModel, TDto> mapper)
        {
            Dtos = new ListResult<TDto>(totalCount, mapper.MapToListItemDto(models));
        }

        public IListResult<TDto> Dtos { get; set; }
    }
}