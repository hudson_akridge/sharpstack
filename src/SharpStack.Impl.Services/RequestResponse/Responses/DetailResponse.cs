﻿using SharpStack.Core.Services.Responses;
using SharpStack.Impl.Services.Dtos;

namespace SharpStack.Impl.Services.RequestResponse.Responses
{
    public class DetailResponse<TModel, TDto> : ResponseBase
        where TModel : class
        where TDto : IDto
    {
        public DetailResponse(TModel model, IMapper<TModel, TDto> mapper)
        {
            MapToDto(model, mapper);
        }

        protected virtual void MapToDto(TModel model, IMapper<TModel, TDto> mapper)
        {
            Dto = mapper.MapToDetailDto(model);
        }

        public TDto Dto { get; set; }
    }
}