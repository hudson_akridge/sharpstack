﻿namespace SharpStack.Impl.Services.Dtos
{
    public interface ITrackedEntityDto : ITrackedClassDto
    {
        string Name { get; set; }
    }
}