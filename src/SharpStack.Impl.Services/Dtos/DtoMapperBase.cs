﻿using System.Collections.Generic;
using System.Linq;
using SharpStack.Impl.Model;

namespace SharpStack.Impl.Services.Dtos
{
    public abstract class DtoMapperBase<TModel, TDto> : IMapper<TModel, TDto>
        where TModel : class
        where TDto : IDto
    {
        public virtual IEnumerable<TDto> MapToDetailDto(IEnumerable<TModel> models)
        {
            return models.Select(MapToDetailDto).ToArray();
        }

        public abstract TDto MapToDetailDto(TModel model);

        public virtual IEnumerable<TDto> MapToListItemDto(IEnumerable<TModel> models)
        {
            return models.Select(MapToListItemDto).ToArray();
        }

        public virtual TDto MapToListItemDto(TModel model)
        {
            return MapToDetailDto(model);
        }

        public abstract void MapToModel(TModel existingModel, TDto dto);

        protected internal virtual void MapBaseInfoToDto(TDto existingDto, TModel model)
        {
            var trackedClass = model as ITrackedClass;
            var classDto = existingDto as ITrackedClassDto;
            if (trackedClass != null && classDto != null)
            {
                classDto.CreatedOn = trackedClass.CreatedOn;
                classDto.Id = trackedClass.Id;
            }

            var entityDto = classDto as ITrackedEntityDto;
            var entity = model as ITrackedEntity;
            if (entityDto != null && entity != null)
            {
                entityDto.Name = entity.Name;
            }
        }

        protected internal virtual void MapBaseInfoToModel(TModel existingModel, TDto dto)
        {
            var entityDto = dto as ITrackedEntityDto;
            var entity = existingModel as ITrackedEntity;
            if (entity != null && entityDto != null)
            {
                entity.Name = entityDto.Name;
            }
        }
    }
}