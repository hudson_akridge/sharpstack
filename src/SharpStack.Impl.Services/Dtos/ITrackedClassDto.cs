﻿using System;

namespace SharpStack.Impl.Services.Dtos
{
    public interface ITrackedClassDto : IDto
    {
        Guid Id { get; set; }
        DateTime CreatedOn { get; set; }
    }
}