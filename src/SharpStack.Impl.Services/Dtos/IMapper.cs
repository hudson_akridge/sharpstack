﻿using System.Collections.Generic;

namespace SharpStack.Impl.Services.Dtos
{
    public interface IMapper<in TModel, TDto>
        where TModel : class
        where TDto : IDto
    {
        IEnumerable<TDto> MapToDetailDto(IEnumerable<TModel> models);
        TDto MapToDetailDto(TModel model);
        IEnumerable<TDto> MapToListItemDto(IEnumerable<TModel> models);
        TDto MapToListItemDto(TModel model);
        void MapToModel(TModel existingModel, TDto dto);
    }
}