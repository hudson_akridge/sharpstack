﻿using System;
using System.Collections.Generic;
using SharpStack.Core.Services.Requests;
using SharpStack.Impl.Model;

namespace SharpStack.Impl.Services.Queries
{
    public interface ITrackedClassQueryObject<TModel, out TQueryObject> where TModel : ITrackedClass
    {
        IListResult<TModel> ListPaged(int start, int limit);
        IListResult<TModel> ListPaged(ListOptions options);
        IEnumerable<TModel> ListAll();
        IEnumerable<Guid> ListAllIds();
        TModel GetSingle();
        TQueryObject RestrictById(Guid id);
        TQueryObject RestrictByIds(IEnumerable<Guid> ids);
        TQueryObject OmitByIds(IEnumerable<Guid> ids);
    }
}