﻿using System.Collections.Generic;

namespace SharpStack.Impl.Services.Queries
{
    public class ListResult<TModel> : IListResult<TModel> where TModel : class
    {
        public ListResult(long totalCount, IEnumerable<TModel> records)
        {
            TotalCount = totalCount;
            Results = records;
        }

        public IEnumerable<TModel> Results { get; set; }
        public long TotalCount { get; set; }
    }

    public interface IListResult<TModel>
    {
        IEnumerable<TModel> Results { get; set; }
        long TotalCount { get; set; }
    }
}