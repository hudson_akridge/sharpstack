﻿using SharpStack.Impl.Model;

namespace SharpStack.Impl.Services.Queries
{
    public interface ITrackedEntityQueryObject<TModel, out TQueryObject> : ITrackedClassQueryObject<TModel, TQueryObject>
        where TModel : ITrackedEntity
    {
        TQueryObject RestrictByName(string name);
        TQueryObject RestrictByNameContains(string name);
        TQueryObject OrderByName(bool ascending = true);
    }
}