﻿using System.Drawing;
using System.IO;

namespace SharpStack.Impl.Services.IO
{
    public interface IFileService
    {
        byte[] GetData(IFileReference file);
        MemoryStream GetDataStream(IFileReference file);
        byte[] GetDataThumbnail(IFileReference file, Size size, out string mimeType);
        void WriteData(IFileReference file, byte[] data);
        void DeleteData(IFileReference file);
    }
}