﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using SharpStack.Impl.EnvironmentConfig;
using SharpStack.Impl.Services.IO.Resources;

namespace SharpStack.Impl.Services.IO
{
    public class FileSystemService : IFileService
    {
        public byte[] GetData(IFileReference file)
        {
            var fileInfo = GetfileFileInfo(file);
            return GetFileAsByteArray(fileInfo.FullName);
        }

        public MemoryStream GetDataStream(IFileReference file)
        {
            var fileInfo = GetfileFileInfo(file);
            return GetFileAsMemoryStream(fileInfo.FullName);
        }

        public byte[] GetDataThumbnail(IFileReference file, Size size, out string mimeType)
        {
            var filename = GetfileFileInfo(file) + string.Format(".thumb_{0}_{1}", size.Width, size.Height);

            if (File.Exists(filename))
            {
                mimeType = file.MimeType;
                return GetFileAsByteArray(filename);
            }

            return GenerateThumbnail(file, size, new FileInfo(filename), out mimeType);
        }

        public void WriteData(IFileReference file, byte[] data)
        {
            var fileInfo = GetfileFileInfo(file);

            file.SizeInBytes = data.Length;
            WriteBytesAsFile(data, fileInfo);
        }

        public void DeleteData(IFileReference file)
        {
            if (file == null) return;

            var fileInfo = GetfileFileInfo(file);
            File.Delete(fileInfo.FullName);

            var files = Directory.GetFiles(fileInfo.DirectoryName, fileInfo.Name + ".*");
            foreach (var f in files)
            {
                File.Delete(f);
            }
        }

        public void DeleteData(IEnumerable<IFileReference> files)
        {
            foreach (var file in files)
            {
                DeleteData(file);
            }
        }

        protected virtual FileInfo GetfileFileInfo(IFileReference file)
        {
            if (string.IsNullOrEmpty(file.GroupKey))
            {
                throw new InvalidOperationException("Cannot save a File that has not been added to a Grouping Type.");
            }

            var rootDirectory = AppConfig.GetConfigElement(AppConfig.RootFileServicePath);
            var path = Path.Combine(rootDirectory, file.GroupKey, file.UniqueId);
            return new FileInfo(path);
        }

        private static byte[] GetFileAsByteArray(string filename)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return fs.ToArray();
            }
        }

        private static MemoryStream GetFileAsMemoryStream(string filename)
        {
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var ms = new MemoryStream();
                fs.WriteTo(ms);
                return ms;
            }
        }

        private static void WriteBytesAsFile(byte[] data, FileInfo filename)
        {
            if (!Directory.Exists(filename.DirectoryName))
            {
                Directory.CreateDirectory(filename.DirectoryName);
            }

            File.WriteAllBytes(filename.FullName, data);
        }

        private static void WriteStreamAsFile(Stream data, FileInfo filename)
        {
            if (!Directory.Exists(filename.DirectoryName))
            {
                Directory.CreateDirectory(filename.DirectoryName);
            }

            data.Position = 0;
            data.SaveAs(filename.FullName, FileMode.Create);
        }

        private byte[] GenerateThumbnail(IFileReference file, Size size, FileInfo fileInfo, out string mimeType)
        {
            var thumbnail = new MemoryStream();

            if (IsResizeSupported(file.MimeType))
            {
                var image = Image.FromStream(GetDataStream(file));
                image = GetResizedImage(image, size);
                image.Save(thumbnail, GetImageFormat(file.MimeType));
                mimeType = file.MimeType;
                WriteStreamAsFile(thumbnail, fileInfo);
            }
            else
            {
                var image = GetfileIcon(file.MimeType);
                image.Save(thumbnail, ImageFormat.Gif);
                mimeType = "image/gif";
            }

            return thumbnail.ToArray();
        }

        private static Image GetfileIcon(string mimeType)
        {
            Bitmap icon;
            if (mimeType.Contains("ms-excel"))
            {
                icon = Icons.ms_excel;
            }
            else if (mimeType.Contains("pdf"))
            {
                icon = Icons.adobe_pdf;
            }
            else if (mimeType.Contains("msword"))
            {
                icon = Icons.ms_word;
            }
            else
            {
                icon = Icons.generic_text;
            }

            var imageStream = new MemoryStream();
            icon.Save(imageStream, ImageFormat.Gif);

            return Image.FromStream(imageStream);
        }

        private static Image GetResizedImage(Image image, Size requestedSize)
        {
            if (requestedSize.Width <= 0 || requestedSize.Height <= 0)
            {
                throw new ArgumentException("height and width must be greater than 0");
            }

            //Adjust requestedSize to rescaledSize with respect to the original aspect ratio
            var widthScale = (double) requestedSize.Width/image.Width;
            var heightScale = (double) requestedSize.Height/image.Height;
            var scale = widthScale <= heightScale ? widthScale : heightScale;

            var rescaledSize = new Size((int) (image.Width*scale),
                (int) (image.Height*scale));

            return new Bitmap(image, rescaledSize);
        }

        private static bool IsResizeSupported(string mimeType)
        {
            return new List<string>
                {
                    "image/bmp",
                    "image/gif",
                    "image/vnd.microsoft.icon",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png",
                    "image/x-png",
                    "image/tiff",
                    "image/wmf"
                }.Contains(mimeType);
        }

        private static ImageFormat GetImageFormat(string mimeType)
        {
            if (mimeType == "image/bmp")
            {
                return ImageFormat.Bmp;
            }
            if (mimeType == "image/gif")
            {
                return ImageFormat.Gif;
            }
            if (mimeType == "image/vnd.microsoft.icon")
            {
                return ImageFormat.Icon;
            }
            if (mimeType == "image/jpeg")
            {
                return ImageFormat.Jpeg;
            }
            if (mimeType == "image/pjpeg")
            {
                return ImageFormat.Jpeg;
            }
            if (mimeType == "image/png")
            {
                return ImageFormat.Png;
            }
            if (mimeType == "image/x-png")
            {
                return ImageFormat.Png;
            }
            if (mimeType == "image/tiff")
            {
                return ImageFormat.Tiff;
            }
            if (mimeType == "image/wmf")
            {
                return ImageFormat.Wmf;
            }
            throw new ArgumentException("Unsupported mimetype", "mimeType");
        }
    }
}