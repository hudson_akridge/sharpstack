﻿using System;

namespace SharpStack.Impl.Services.IO
{
    public interface IFileReference
    {
        string GroupKey { get; }
        string UniqueId { get; }
        string MimeType { get; set; }
        long SizeInBytes { get; set; }
        DateTime CreatedOn { get; }
    }
}