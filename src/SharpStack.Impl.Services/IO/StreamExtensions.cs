﻿using System.IO;

namespace SharpStack.Impl.Services.IO
{
    public static class StreamExtensions
    {
        public static void SaveAs(this Stream stream, string path, FileMode mode)
        {
            using (var fs = new FileStream(path, mode))
            {
                stream.WriteTo(fs);
            }
        }

        public static void WriteTo(this Stream source, Stream dest)
        {
            var buffer = new byte[32*1024];
            int read;
            while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
            {
                dest.Write(buffer, 0, read);
            }
        }

        public static byte[] ToArray(this Stream stream)
        {
            var data = new byte[stream.Length];
            stream.Read(data, 0, (int) stream.Length);
            return data;
        }
    }
}