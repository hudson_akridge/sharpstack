﻿using System;
using SharpStack.Impl.Model;
using SharpStack.Security;

namespace SharpStack.Impl.Services.Security
{
    public class AuthorizedUser : TrackedEntity, IAuthorizedUser
    {
        public string UserId
        {
            get { return base.Id.ToString(); }
            set { base.Id = new Guid(value); }
        }

        public string UserName
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        public virtual string Email { get; set; }
        public virtual string HashedPassword { get; set; }
        public virtual string Salt { get; set; }
        public virtual Guid? ResetPasswordCode { get; set; }
        public virtual DateTime? ResetPasswordExpDate { get; set; }
    }
}