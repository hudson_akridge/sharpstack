﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using SharpStack.Core.Emails;
using SharpStack.Impl.EnvironmentConfig;
using SharpStack.Security;
using log4net;

namespace SharpStack.Impl.Services.Emails
{
    public class EmailManager : IEmailManager
    {
        private static readonly ILog Log = LogManager.GetLogger(AppConfig.GetConfigElement(AppConfig.EmailLog));
        private static readonly string SmtpServer = AppConfig.GetConfigElement(AppConfig.SmtpServer);
        private static readonly string SmtpServerPort = AppConfig.GetConfigElement(AppConfig.SmtpServerPort);
        private static readonly bool SmtpUseSsl = bool.Parse(AppConfig.GetConfigElement(AppConfig.SmtpUseSsl));
        private static readonly string SmtpUserName = AppConfig.GetConfigElement(AppConfig.SmtpUserName);
        private static readonly string SmtpPassword = AppConfig.GetConfigElement(AppConfig.SmtpPassword);
        private static readonly string FromEmail = AppConfig.GetConfigElement(AppConfig.SmtpFromEmail);
        private static readonly string AdminEmail = AppConfig.GetConfigElement(AppConfig.AdminEmail);
        private static readonly string TestMode = AppConfig.GetConfigElement(AppConfig.InTestMode);
        private static readonly string TestModeEmail = AppConfig.GetConfigElement(AppConfig.TestModeEmail);
        public static string NoReplySender = AppConfig.GetConfigElement(AppConfig.NoReplySender);

        public void SendForgotPasswordMessage(IAuthorizedUser user)
        {
            var email = new ForgotPasswordMailMessage(user);
            email.To.Add(user.Email);
            SendAsyncEmailCore(email);
        }

        public void SendAsyncEmail(MailMessage email)
        {
            SendAsyncEmailCore(email);
        }

        public void SendAsyncEmail(MailMessage email, IEnumerable<Attachment> attachments)
        {
            AddAttachments(email, attachments);

            SendAsyncEmailCore(email);
        }

        private void AddAttachments(MailMessage email, IEnumerable<Attachment> attachments)
        {
            var attachmentSafeArray = attachments as Attachment[] ?? attachments.ToArray();
            var totalSizeOfAttachments = attachmentSafeArray.Sum(attachment => attachment.ContentStream.Length);
            if (totalSizeOfAttachments <= 10000000)
            {
                foreach (var attachment in attachmentSafeArray)
                {
                    email.Attachments.Add(attachment);
                }
                return;
            }

            //We want to notify the user that attachments were failed to be sent out due to file size.
            if (!email.To.Any())
            {
                return;
            }
            var attachmentsEmail = new MailMessage(new MailAddress(AdminEmail), email.To.First())
                {
                    Subject = "Large attachment failure: " + email.Subject,
                    Body = @"There are attachments that are too large to be sent out in the email, please log into the application to view the attachments."
                };
            SendAsyncEmailCore(attachmentsEmail);
        }

        private void SendAsyncEmailCore(MailMessage email)
        {
            CopyAdminOnEmail(email);
            UseOnlyTestAddressWhenInTestMode(email);

            if (email.To.Any(to => string.IsNullOrEmpty(to.Address)))
            {
                throw new ArgumentNullException("email.To.Address", "To.Address is empty.");
            }

            //All email messages need to be from the login name or else the mail Daemon will refuse it
            email.From = new MailAddress(FromEmail);

            SendEmailAwaitable(email);
        }

        public static async Task<bool> SendEmailAwaitable(MailMessage message)
        {
            try
            {
                var smtpClient = GetNewSmtpClient();
                smtpClient.Send(message);
                await Task.Yield();
            }
            catch (Exception ex)
            {
                LogError(message, ex);
            }

            return true;
        }

        private void CopyAdminOnEmail(MailMessage email)
        {
            var isAdminNotification = email.To.Any(x => x.Address.Equals(AdminEmail, StringComparison.InvariantCultureIgnoreCase));
            if (!isAdminNotification)
            {
                email.CC.Add(AdminEmail);
            }
        }

        private void UseOnlyTestAddressWhenInTestMode(MailMessage message)
        {
            if (!IsInTestMode()) return;

            message.To.Clear();
            message.CC.Clear();
            message.To.Add(TestModeEmail);
        }

        private bool IsInTestMode()
        {
            bool testMode;
            bool.TryParse(TestMode, out testMode);
            return testMode;
        }

        private static void SmtpClientSendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error == null) return;

            var message = sender as MailMessage;
            if (message == null)
            {
                LogError(e.Error);
                return;
            }
            LogError(message, e.Error);
        }

        private static void LogError(Exception ex)
        {
            Log.Error("Error sending mail.", ex);
        }

        public static void LogError(MailMessage message, Exception ex)
        {
            var to = message.To.FirstOrDefault() ?? new MailAddress("Null@Address.com");
            var cc = message.CC.FirstOrDefault() ?? new MailAddress("Null@Address.com");
            var attachments = message.Attachments.Count;

            Log.Error(string.Format("Email To:{0}, CC:{1}, Attachments:{2}, IsHtml:{3}", to.Address, cc.Address, attachments, message.IsBodyHtml), ex);
        }

        private static SmtpClient GetNewSmtpClient()
        {
            return new SmtpClient(SmtpServer, int.Parse(SmtpServerPort))
                {
                    Credentials = new NetworkCredential(SmtpUserName, SmtpPassword),
                    EnableSsl = SmtpUseSsl,
                };
        }
    }
}