﻿using System.Net.Mail;
using SharpStack.Impl.EnvironmentConfig;
using SharpStack.Security;

namespace SharpStack.Impl.Services.Emails
{
    public class ForgotPasswordMailMessage : MailMessage
    {
        private readonly string _passwordResetUrl = AppConfig.GetConfigElement(AppConfig.PasswordReset);

        public ForgotPasswordMailMessage(IAuthorizedUser user)
        {
            Subject = "Reset your password";
            IsBodyHtml = true;

            Body = @"<p>Forgot your password, " + user.UserName + "?</p>" +
                   "<p>We received a request to reset your password.</p>" +
                   "<p>To reset your password, click the link below (or copy and paste URL into your browser)</p>" +
                   _passwordResetUrl + "?userName=" + user.UserName + "&token=" + user.ResetPasswordCode;
        }
    }
}