﻿using System;
using System.Collections.Generic;

namespace SharpStack.Security.Queries
{
    public interface IAuthorizedUserQueryObject
    {
        IAuthorizedUserQueryObject RestrictByAuthUserId(Guid id);
        IAuthorizedUserQueryObject RestrictByAuthUserIds(IEnumerable<Guid> ids);
        IAuthorizedUserQueryObject RestrictByAuthUserName(string userName);
        IAuthorizedUserQueryObject RestrictByAuthUserEmail(string email);
        IEnumerable<IAuthorizedUser> ListAllAuthUsers();
        IAuthorizedUser GetSingleAuthUser();
    }
}