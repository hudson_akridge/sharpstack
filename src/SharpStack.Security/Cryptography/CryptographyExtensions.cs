﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SharpStack.Security.Cryptography
{
    public static class CryptographyExtensions
    {
        public static bool MatchesAfterHashing(this string existingHash, string plainText, string salt)
        {
            var mintedHash = plainText.HashText(salt);
            return mintedHash == existingHash;
        }

        public static string HashText(this string unhashedText, string salt)
        {
            return SHA256Hash(unhashedText, salt);
        }

        public static byte[] HexToByte(this string hex)
        {
            var bytes = new byte[hex.Length/2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hex.Substring(i*2, 2), 16);
            }

            return bytes;
        }

        private static string SHA256Hash(string password, string salt)
        {
            var sha = SHA256.Create();
            var hash = sha.ComputeHash(Encoding.Unicode.GetBytes(password + salt));
            return BitConverter.ToString(hash).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Generates a salt key that's 8 characters by default. If you need to specify a character count, use the overload.
        /// </summary>
        /// <returns></returns>
        public static string GenerateSalt()
        {
            return GenerateSalt(8);
        }

        public static string GenerateSalt(int length)
        {
            var builder = new StringBuilder();
            var random = new Random();
            for (var i = 0; i < length; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(32 + random.NextDouble()*94)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}