﻿using System;

namespace SharpStack.Security
{
    public interface IAuthorizedUser
    {
        string UserId { get; set; }
        string UserName { get; set; }
        string Email { get; set; }
        string HashedPassword { get; set; }
        string Salt { get; set; }
        Guid? ResetPasswordCode { get; set; }
        DateTime? ResetPasswordExpDate { get; set; }
    }
}