﻿namespace SharpStack.Security
{
    public interface IMembershipService
    {
        int MinPasswordLength { get; }
        bool ValidateUser(string userName, string password);
        MembershipCreateStatuses CreateUser(string userName, string password, string email);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
    }

    public enum MembershipCreateStatuses
    {
        DuplicateEmail,
        DuplicateProviderUserKey,
        DuplicateUserName,
        InvalidAnswer,
        InvalidEmail,
        InvalidPassword,
        InvalidProviderUserKey,
        InvalidQuestion,
        InvalidUserName,
        ProviderError,
        Success,
        UserRejected,
    }
}