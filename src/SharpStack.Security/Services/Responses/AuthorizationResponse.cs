﻿using SharpStack.Core.Services.Responses;

namespace SharpStack.Security.Services.Responses
{
    public class AuthenticationResponse : ResponseBase
    {
        public static AuthenticationResponse Success
        {
            get { return new AuthenticationResponse(); }
        }

        public static AuthenticationResponse Failure
        {
            get
            {
                return (AuthenticationResponse) new AuthenticationResponse()
                                                    .WithErrors(new[] {"There was a problem with your username or password."});
            }
        }
    }
}