﻿using System;
using SharpStack.Core.Services.Requests;

namespace SharpStack.Security.Services.Requests
{
    [Serializable]
    public class ForgotPasswordResetRequest : IRequest
    {
        public ForgotPasswordResetRequest()
        {
        }

        public ForgotPasswordResetRequest(string userName, Guid? forgotPasswordCode)
        {
            UserName = userName;
            ForgotPasswordCode = forgotPasswordCode;
        }

        public string UserName { get; set; }
        public Guid? ForgotPasswordCode { get; set; }
    }
}