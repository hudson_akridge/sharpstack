﻿using System;
using SharpStack.Core.Services.Requests;

namespace SharpStack.Security.Services.Requests
{
    [Serializable]
    public class ForgotPasswordRequest : IRequest
    {
        public ForgotPasswordRequest()
        {
        }

        public ForgotPasswordRequest(string userName)
        {
            UserName = userName;
        }

        public string UserName { get; set; }
    }
}