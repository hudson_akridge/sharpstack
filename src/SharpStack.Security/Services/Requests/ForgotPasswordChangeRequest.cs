﻿using System;
using System.Xml.Serialization;
using SharpStack.Core.Services.Requests;

namespace SharpStack.Security.Services.Requests
{
    [Serializable]
    public class ForgotPasswordChangeRequest : IRequest
    {
        public ForgotPasswordChangeRequest()
        {
        }

        public ForgotPasswordChangeRequest(string userName, Guid? forgotPasswordCode, string password)
        {
            UserName = userName;
            ForgotPasswordCode = forgotPasswordCode;
            NewPassword = password;
        }

        public string UserName { get; set; }
        public Guid? ForgotPasswordCode { get; set; }

        [XmlIgnore]
        public String NewPassword { get; set; }
    }
}