﻿using System;
using SharpStack.Core;
using SharpStack.Core.Services.Handlers;
using SharpStack.Core.Services.Requests;
using SharpStack.Core.UnitsOfWork;

namespace SharpStack.Security.Services.Handlers
{
    public abstract class ChangePasswordHandlerBase<T> : HandlerBase<T> where T : IRequest
    {
        protected ChangePasswordHandlerBase(IUnitOfWork uow)
            : base(uow)
        {
        }

        protected static bool AuthenticateResetToken(IAuthorizedUser user, Guid resetToken)
        {
            // All resetToken fields must be properly set
            if (user == null || !user.ResetPasswordCode.HasValue || !user.ResetPasswordExpDate.HasValue)
            {
                return false;
            }

            // resetToken must be the same as what we have, and the token must not have expired.
            return user.ResetPasswordCode.Value.CompareTo(resetToken) == 0
                   && user.ResetPasswordExpDate.Value.CompareTo(DateTime.Now) > 0;
        }
    }
}