﻿using System;
using SharpStack.Core;
using SharpStack.Core.Services.Handlers;
using SharpStack.Core.Services.Responses;
using SharpStack.Core.UnitsOfWork;
using SharpStack.Security.Emails;
using SharpStack.Security.Queries;
using SharpStack.Security.Services.Requests;
using SharpStack.Security.Services.Responses;

namespace SharpStack.Security.Services.Handlers
{
    public class ForgotPasswordHandler : HandlerBase<ForgotPasswordRequest>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISecurityEmailManager _emailManager;
        private readonly IAuthorizedUserQueryObject _authorizedUserQueryObject;

        public ForgotPasswordHandler(IUnitOfWork unitOfWork, ISecurityEmailManager emailManager, IAuthorizedUserQueryObject authorizedUserQueryObject)
            : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _emailManager = emailManager;
            _authorizedUserQueryObject = authorizedUserQueryObject;
        }

        public override IResponse Handle(ForgotPasswordRequest request)
        {
            try
            {
                var user = _authorizedUserQueryObject
                    .RestrictByAuthUserName(request.UserName)
                    .GetSingleAuthUser();

                user.ResetPasswordCode = Guid.NewGuid();
                user.ResetPasswordExpDate = DateTime.Now.AddHours(24);
                _unitOfWork.Save(user);

                _emailManager.SendForgotPasswordMessage(user);

                return AuthenticationResponse.Success;
            }
            catch (Exception ex)
            {
                return AuthenticationResponse.Failure.WithErrors(new[] {ex.Message});
            }
        }
    }
}