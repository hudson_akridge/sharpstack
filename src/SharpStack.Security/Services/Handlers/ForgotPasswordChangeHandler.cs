﻿using SharpStack.Core;
using SharpStack.Core.Services.Responses;
using SharpStack.Core.UnitsOfWork;
using SharpStack.Security.Cryptography;
using SharpStack.Security.Queries;
using SharpStack.Security.Services.Requests;
using SharpStack.Security.Services.Responses;

namespace SharpStack.Security.Services.Handlers
{
    public class ForgotPasswordChangeHandler : ChangePasswordHandlerBase<ForgotPasswordChangeRequest>
    {
        private readonly IAuthorizedUserQueryObject _authorizedUserQueryObject;

        public ForgotPasswordChangeHandler(IUnitOfWork unitOfWork, IAuthorizedUserQueryObject authorizedUserQueryObject)
            : base(unitOfWork)
        {
            _authorizedUserQueryObject = authorizedUserQueryObject;
        }

        public override IResponse Handle(ForgotPasswordChangeRequest request)
        {
            if (!request.ForgotPasswordCode.HasValue)
            {
                return AuthenticationResponse.Failure;
            }

            var user = _authorizedUserQueryObject
                .RestrictByAuthUserName(request.UserName)
                .GetSingleAuthUser();

            if (AuthenticateResetToken(user, request.ForgotPasswordCode.Value))
            {
                user.Salt = CryptographyExtensions.GenerateSalt();
                user.HashedPassword = request.NewPassword.HashText(user.Salt);
                user.ResetPasswordCode = null;
                user.ResetPasswordExpDate = null;

                Uow.Save(user);
                return AuthenticationResponse.Success;
            }

            return AuthenticationResponse.Failure;
        }
    }
}