﻿using SharpStack.Core;
using SharpStack.Core.Services.Responses;
using SharpStack.Core.UnitsOfWork;
using SharpStack.Security.Queries;
using SharpStack.Security.Services.Requests;
using SharpStack.Security.Services.Responses;

namespace SharpStack.Security.Services.Handlers
{
    public class ForgotPasswordResetHandler : ChangePasswordHandlerBase<ForgotPasswordResetRequest>
    {
        private readonly IAuthorizedUserQueryObject _authorizedUserQueryObject;

        public ForgotPasswordResetHandler(IUnitOfWork unitOfWork, IAuthorizedUserQueryObject authorizedUserQueryObject)
            : base(unitOfWork)
        {
            _authorizedUserQueryObject = authorizedUserQueryObject;
        }

        public override IResponse Handle(ForgotPasswordResetRequest request)
        {
            if (!request.ForgotPasswordCode.HasValue)
            {
                return AuthenticationResponse.Failure;
            }

            var u = _authorizedUserQueryObject
                .RestrictByAuthUserName(request.UserName)
                .GetSingleAuthUser();

            return AuthenticateResetToken(u, request.ForgotPasswordCode.Value)
                       ? AuthenticationResponse.Success
                       : AuthenticationResponse.Failure;
        }
    }
}