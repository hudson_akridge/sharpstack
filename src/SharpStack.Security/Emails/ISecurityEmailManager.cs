﻿namespace SharpStack.Security.Emails
{
    public interface ISecurityEmailManager
    {
        void SendForgotPasswordMessage(IAuthorizedUser user);
    }
}