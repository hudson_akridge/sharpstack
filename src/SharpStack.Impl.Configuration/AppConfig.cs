﻿using System.Configuration;

namespace SharpStack.Impl.EnvironmentConfig
{
    /// <summary>
    /// TODO: This is a really basic hack to have a centralized place for config options. Look to change this into an actual config file library
    /// </summary>
    public class AppConfig
    {
        public const string PrefixConfiguration = "SharpStack.";
        public static string SqlServer = PrefixConfiguration + "SqlServer";
        public static string DbName = PrefixConfiguration + "DbName";
        public static string UserName = PrefixConfiguration + "UserName";
        public static string Password = PrefixConfiguration + "Password";
        public static string PasswordReset = PrefixConfiguration + "ActionUrl.PasswordReset";
        public static string RootFileServicePath = PrefixConfiguration + "RootFileServicePath";

        public const string PrefixMail = "Mail.";
        public static string SmtpServer = PrefixConfiguration + PrefixMail + "SmtpServer";
        public static string SmtpServerPort = PrefixConfiguration + PrefixMail + "SmtpServerPort";
        public static string SmtpUseSsl = PrefixConfiguration + PrefixMail + "SmtpUseSsl";
        public static string SmtpUserName = PrefixConfiguration + PrefixMail + "SmtpUserName";
        public static string SmtpPassword = PrefixConfiguration + PrefixMail + "SmtpPassword";
        public static string SmtpFromEmail = PrefixConfiguration + PrefixMail + "SmtpFromEmail";
        public static string AdminEmail = PrefixConfiguration + PrefixMail + "AdminEmail";
        public static string InTestMode = PrefixConfiguration + PrefixMail + "InTestMode";
        public static string TestModeEmail = PrefixConfiguration + PrefixMail + "TestModeEmail";
        public static string NoReplySender = PrefixConfiguration + PrefixMail + "NoReplySender";

        public const string PrefixLog = "Log.";
        public static string EmailLog = PrefixConfiguration + PrefixLog + "EmailNotifications";


        public static string GetConfigElement(string elementName)
        {
            return ConfigurationManager.AppSettings[elementName];
        }
    }
}