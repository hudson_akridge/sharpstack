﻿using System.Collections.Generic;
using System.Net.Mail;

namespace SharpStack.Core.Emails
{
    public interface IEmailManager
    {
        void SendAsyncEmail(MailMessage email);
        void SendAsyncEmail(MailMessage email, IEnumerable<Attachment> attachments);
    }
}