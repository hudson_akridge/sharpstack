﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace SharpStack.Core
{
    public static class PropertyReflector
    {
        public static string GetName<TModel>(Expression<Func<TModel, object>> property)
        {
            return GetMemberInfo(property).Name;
        }

        private static MemberInfo GetMemberInfo<TEntity>(Expression<Func<TEntity, object>> expression)
        {
            MemberInfo memberInfo = null;
            var expressionType = expression.Body.NodeType;
            if (expressionType != ExpressionType.Convert)
            {
                if (expressionType != ExpressionType.MemberAccess)
                {
                    throw new ArgumentException("Unsupported ExpressionType", "expression");
                }
            }
            else
            {
                var body = (UnaryExpression) expression.Body;
                var operand = body.Operand as MethodCallExpression;
                if (operand != null)
                {
                    memberInfo = operand.Method;
                }
                else
                {
                    var memberExpression = body.Operand as MemberExpression;
                    if (memberExpression != null)
                    {
                        memberInfo = memberExpression.Member;
                    }
                }
            }

            if (memberInfo == null)
            {
                memberInfo = ((MemberExpression) expression.Body).Member;
            }

            if (memberInfo == null)
            {
                throw new ArgumentException("Could not locate MemberInfo.", "expression");
            }

            return memberInfo;
        }
    }
}