﻿using System;

namespace SharpStack.Core.Conversions
{
    public static class DateConverter
    {
        public static DateTime? ToDateTime(string dateText, string timeText)
        {
            if (dateText == null)
            {
                return null;
            }
            var date = DateTime.Parse(dateText);

            if (!string.IsNullOrEmpty(timeText))
            {
                timeText = " " + timeText;
            }

            var dateTimeText = ToDateString(date) + timeText;
            return DateTime.Parse(dateTimeText);
        }

        public static DateTime? ToDate(string date)
        {
            return (string.IsNullOrEmpty(date))
                       ? null
                       : new DateTime?(DateTime.Parse(date));
        }

        public static string ToDateString(DateTime? date)
        {
            return (date.HasValue)
                       ? date.Value.ToShortDateString()
                       : string.Empty;
        }

        public static string ToDateTimeString(DateTime? date)
        {
            return (date.HasValue)
                       ? date.Value.ToString()
                       : string.Empty;
        }

        public static string ToTimeString(DateTime? time)
        {
            return (time.HasValue)
                       ? string.Format("{0:t}", time.Value)
                       : string.Empty;
        }
    }
}