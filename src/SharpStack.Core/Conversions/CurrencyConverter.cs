﻿using System;
using System.Globalization;

namespace SharpStack.Core.Conversions
{
    public static class CurrencyConverter
    {
        public static string ToString(decimal value)
        {
            return value.ToString("C");
        }

        public static decimal ToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = "0";
            }

            return Math.Round(Decimal.Parse(value, NumberStyles.Currency), 2);
        }
    }
}