﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SharpStack.Core.UnitsOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        T Load<T>(Guid id);
        SqlConnection GetConnection();
        void Save(object entity);
        void Save(IEnumerable<object> entities);
        void Delete(object entity);
        void Delete(IEnumerable<object> entities);
        void CommitTransaction();
        void FlushToDatabase();
        string UserName { get; }
    }
}