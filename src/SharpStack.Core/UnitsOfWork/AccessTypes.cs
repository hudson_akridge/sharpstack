﻿namespace SharpStack.Core.UnitsOfWork
{
    public enum AccessTypes
    {
        ReadWrite,
        ReadOnly
    }
}