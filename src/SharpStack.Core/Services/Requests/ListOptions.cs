﻿using System;

namespace SharpStack.Core.Services.Requests
{
    [Serializable]
    public class ListOptions
    {
        public int Start { get; set; }
        public int Limit { get; set; }

        public static readonly ListOptions Default = new ListOptions {Start = 0, Limit = int.MaxValue};
    }
}