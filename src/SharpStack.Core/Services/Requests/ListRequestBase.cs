﻿using System;

namespace SharpStack.Core.Services.Requests
{
    [Serializable]
    public abstract class ListRequestBase : RequestBase
    {
        protected ListRequestBase()
            : this(ListOptions.Default)
        {
        }

        protected ListRequestBase(ListOptions listOptions)
        {
            ListOptions = listOptions ?? ListOptions.Default;
        }

        public ListOptions ListOptions { get; set; }
    }
}