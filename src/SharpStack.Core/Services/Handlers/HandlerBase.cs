﻿using SharpStack.Core.Services.Requests;
using SharpStack.Core.Services.Responses;
using SharpStack.Core.UnitsOfWork;

namespace SharpStack.Core.Services.Handlers
{
    public abstract class HandlerBase<T>
        : IHandler<T> where T : IRequest
    {
        protected readonly IUnitOfWork Uow;

        protected HandlerBase(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public abstract IResponse Handle(T request);
    }
}