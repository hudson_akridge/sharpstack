﻿using SharpStack.Core.Services.Requests;
using SharpStack.Core.Services.Responses;

namespace SharpStack.Core.Services.Handlers
{
    public interface IHandler<in T> where T : IRequest
    {
        IResponse Handle(T request);
    }
}