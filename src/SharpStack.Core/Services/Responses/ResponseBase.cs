﻿using System.Collections.Generic;
using System.Linq;

namespace SharpStack.Core.Services.Responses
{
    public abstract class ResponseBase : IResponse
    {
        private readonly List<LogEntry> _resultLog = new List<LogEntry>();

        public bool HadErrors
        {
            get { return _resultLog.Where(x => x.Severity == Severity.Error).Any(); }
        }

        public IEnumerable<LogEntry> ResultLog
        {
            get { return _resultLog.AsReadOnly(); }
        }

        public IResponse WithErrors(IEnumerable<string> errors)
        {
            _resultLog.AddRange(errors.Select(x => new LogEntry(Severity.Error, x)));
            return this;
        }

        public IResponse WithWarnings(IEnumerable<string> warnings)
        {
            _resultLog.AddRange(warnings.Select(x => new LogEntry(Severity.Warning, x)));
            return this;
        }

        public IResponse WithInformation(IEnumerable<string> info)
        {
            _resultLog.AddRange(info.Select(x => new LogEntry(Severity.Info, x)));
            return this;
        }
    }

    public class LogEntry
    {
        public LogEntry(Severity severity, string message)
        {
            Severity = severity;
            Message = message;
        }

        public Severity Severity { get; private set; }
        public string Message { get; private set; }

        public override string ToString()
        {
            return Severity + " - " + Message;
        }
    }

    public enum Severity
    {
        Error,
        Warning,
        Info
    }
}