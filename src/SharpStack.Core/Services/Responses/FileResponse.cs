﻿using System;

namespace SharpStack.Core.Services.Responses
{
    [Serializable]
    public class FileResponse : IResponse
    {
        public FileResponse(byte[] data, string mimeType, string fileName)
        {
            Data = data;
            MimeType = mimeType;
            FileName = fileName;
        }

        public byte[] Data { get; private set; }
        public string MimeType { get; private set; }
        public string FileName { get; private set; }
    }
}