﻿using System;

namespace SharpStack.Core.Services.Responses
{
    [Serializable]
    public class EmptyResponse : ResponseBase
    {
    }
}