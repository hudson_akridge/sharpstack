﻿using System;

namespace SharpStack.Core.Services.Responses
{
    public class NewEntityResponse : IResponse
    {
        public NewEntityResponse(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}